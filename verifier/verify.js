const jwt = require("jsonwebtoken");
const { execSync } = require("child_process");

const certToPubKey = (certPath) =>
  execSync(`openssl x509 -pubkey -noout -in ${certPath}`).toString();

const pubKey = certToPubKey("verifier/signer.crt");
const originalPayload = {
  transactionId: "aaa",
  amount: 100.0,
};

const detachedToken = process.argv[2];

const [headerB64, , signature] = detachedToken.split(".");

const header = JSON.parse(Buffer.from(headerB64, "base64").toString("utf8"));

const token =
  headerB64 +
  "." +
  Buffer.from(JSON.stringify(originalPayload)).toString("base64") +
  "." +
  signature;

try {
  const decoded = jwt.verify(token, pubKey, { algorithms: [header["alg"]] });
  console.log("Verified");
} catch (error) {
  console.log("Not verified: ", error.message);
}
