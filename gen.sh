# CA PART
# Generate root CA key & cert
openssl genrsa -out ca/rootCA.key 2048
openssl req -new -x509 -key ca/rootCA.key -sha256 -subj '/C=LT/O=kevin/CN=MyCA' -days 3650 -out ca/rootCA.crt

# Generate intermediate key & cert
openssl genrsa -out ca/intermediateCA.key 2048
openssl req -new -key ca/intermediateCA.key -out ca/intermediateCA.csr -subj '/C=LT/O=kevin/CN=MyCA'
openssl x509 -req -in ca/intermediateCA.csr -CA ca/rootCA.crt -CAkey ca/rootCA.key -CAcreateserial -out ca/intermediateCA.crt -days 500 -sha256

# Generate client key & cert
openssl genrsa -out signer/signer.key 2048
openssl req -new -key signer/signer.key -out signer/signer.csr -subj '/C=PL/O=partner/CN=MyCA'
openssl x509 -req -in signer/signer.csr -CA ca/intermediateCA.crt -CAkey ca/intermediateCA.key -CAcreateserial -out ca/signer.crt -days 500 -sha256

# Distribute certs
cp ca/rootCA.crt verifier/
cp ca/intermediateCA.crt verifier/
cp ca/signer.crt verifier/
