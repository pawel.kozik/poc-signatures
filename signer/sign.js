const jwt = require("jsonwebtoken");
const fs = require("fs");

const privateKey = fs.readFileSync("./signer/signer.key", "utf8");

const payload = {
  transactionId: "aaa",
  amount: 100.0,
};

const token = jwt.sign(payload, privateKey, {
  algorithm: "PS256",
  header: {
    mat: Date.now(),
    mid: "unique-id",
    eid: "partner-x-id",
    crit: ["mat", "mid", "omid"],
  },
  noTimestamp: true, // omit iat in the payload
});

const segments = token.split(".");
const detachedToken = segments[0] + ".." + segments[2];

console.log(detachedToken);
